package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithShield implements DefenseBehavior {
    //ToDo: Complete me
    public DefendWithShield(){

    }
    public String Defend(){
        return "I defend myself with shield";
    }
    public String getType(){
        return "DefendWithShield";
    }
}
