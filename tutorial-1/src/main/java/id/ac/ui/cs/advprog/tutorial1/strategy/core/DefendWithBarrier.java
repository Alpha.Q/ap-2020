package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithBarrier implements DefenseBehavior {
    //ToDo: Complete me
    public DefendWithBarrier(){

    }
    public String Defend(){
        return "I defend myself with barrier";
    }
    public String getType(){
        return "DefendWithBarrier";
    }
}
