package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithArmor implements DefenseBehavior {
    //ToDo: Complete me
    public DefendWithArmor(){

    }
    public String Defend(){
        return "I defend myself with armor";
    }
    public String getType(){
        return "DefendWithArmor";
    }
}
